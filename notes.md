## How to re-deploy the site

## 1. Download the GCloud SDK

Follow the instructions on the site

## 2. Login with gcloud

```bash
gcloud auth application-default login
```

## 3. Create a project

```bash
gcloud projects create ngnr-club
```

set is as default

```bash
gcloud config set project ngnr-club
```

## 4. Link project to billing account

```bash
gcloud beta billing projects link ngnr-club --billing-account $BILLING_ACCOUNT_ID
```

## 5. Link project to billing account

```bash
gcloud beta billing projects link ngnr-club --billing-account $BILLING_ACCOUNT_ID
```

## 6. Enable the APIs on the project

```bash
gcloud services enable container.googleapis.com compute.googleapis.com deploymentmanager.googleapis.com
```

## 7. Create a service account

```bash
gcloud iam service-accounts create test-fishr --display-name "Fishr Test SA"

```

### 8. Set its role to project editor

```bash
# get the policy member
gcloud iam service-accounts list | grep "Fishr Test SA" | awk '{print $4}'

# set the role
gcloud iam service-accounts add-iam-policy-binding $SA_MEMBER --role='roles/editor' --member='serviceAccount:'$SA_MEMBER
```

### 9. Generate a key for the SA

```bash
gcloud iam service-accounts keys list --iam-account=$SA_MEMBER
```

### 10. On terraform, update the path to the file

```terraform
provider "google" {
  project = "ngnr-club"
  # ...
  credentials = file("key-file.json")
}
```

### 11. Apply and boom

```bash
tf apply -auto-approve
```
