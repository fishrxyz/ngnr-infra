# Copyright (c) HashiCorp, Inc.
# SPDX-License-Identifier: MPL-2.0

output "region" {
  value       = "us-central1"
  description = "GCloud Region"
}

output "project_id" {
  value       = "ngnr-club"
  description = "GCloud Project ID"
}

output "cluster_name" {
  value       = google_container_cluster.ngnr_cluster.name
  description = "GKE Cluster Name"
}

output "cluster_host" {
  value       = google_container_cluster.ngnr_cluster.endpoint
  description = "GKE Cluster Host"
}
