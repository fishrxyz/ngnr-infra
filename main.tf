# Login using gcloud
# Create a project 
# Set it as the default
# link the billing account
# enable the APIs on that projct
# create a service acount -> Role: Project -> Editor
# download the creds file
# tf apply
# pull kubeconfig file
# move on 

terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.57.0"
    }
  }
}

provider "google" {
  project = "ngnr-club"
  region = "us-central1"
  zone = "us-central1-c"
  credentials = file("terraform-ngnr-club-sa.json")
}

## Create the VPC first
resource "google_compute_network" "ngnr_vpc" {
  name = "ngnr-club-vpc"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "ngnr_subnet" {
  name = "ngnr-club-subnet"
  network = google_compute_network.ngnr_vpc.name
  ip_cidr_range = "10.10.0.0/24"
}

## Create the cluster _sans_ managed node pool
resource "google_container_cluster" "ngnr_cluster" {
  name = "ngnr-gke"
  remove_default_node_pool = true
  initial_node_count = 1

  network = google_compute_network.ngnr_vpc.name
  subnetwork = google_compute_subnetwork.ngnr_subnet.name
}

resource "google_container_node_pool" "ngnr_nodes" {
  name = google_container_cluster.ngnr_cluster.name
  cluster = google_container_cluster.ngnr_cluster.name
  node_count = 3
  node_config {
    oauth_scopes = [ 
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/cloud-platform",
    ]

    machine_type = "n2-standard-2"
  }
}
