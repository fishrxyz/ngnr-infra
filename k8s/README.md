## Deploy the cluster

Once you know the cluster is ready and up and running, follow the steps
outlined below to deploy the application.

## 1. Create a namespace

```bash
k create ns ngnr-club
```

## 2. Deploy the PVC

```bash
k apply -f pvc.k8s.yaml
```

## 3. Deploy the DB

```bash
helm install -f ./helm/postgres.values.yaml db bitnami/postgresql
```

## 4. Create AWS IAM User and Policy

```bash
aws iam create-user --user-name fishr-ngnr-club
aws iam create-access-key --user-name fishr-ngnr-club

echo "!!!! SAVE_THE_CREDS_YOU_MUG !!!!"

aws iam create-policy --policy-name ngnr-s3 --policy-document file://s3-policy.json
aws iam attach-user-policy --user-name fishr-ngnr-club --policy-arn arn
```

## 5. Create the Config Map and Secret

```bash
k apply -f configmap.k8s.yaml
k create secret generic ngnr-club
# use the following values

```

## 6. Create the secret file

create a file called secret.env with the following keys

```bash
AWS_ACCESS_KEY_ID=bla
AWS_REGION=bla
AWS_SECRET_ACCESS_KEY=bla
MAILCHIMP_URL=bla
NGNR_SECRET_KEY=bla
SENDGRID_API_KEY=bla
```

then run

```bash
k create secret generic ngnr-club --from-env-file=secret.env
```

## 7. Deploy the API

```bash
k apply -f api.deploy.k8s.yaml
```

## 8. Expose the API

```bash
k apply -f api.svc.k8s.yaml
```

## 8. Deploy the UI

```bash
k apply -f ui.deploy.k8s.yaml
```

## 9. Expose the UI

```bash
k apply -f ui.svc.k8s.yaml
```

## 10. Install nginx ingress

```bash
helm install ing ingress-nginx/ingress-nginx -n nginx --create-namespace
```

## 11. Update the A Record on NS

```bash
k get svc -n nginx # to extract the IP
```

## 11. Create the ingress

```bash
k create ing ngnr --rule=ngnr.club/*=ui:8080 --rule=ngnr.club/api*=api:9310 --class=nginx
```

## 12. Install cert-manager

**NOTE: INSTALL THE FUCKING CRDs**

```bash
helm install cert-manager --namespace cert-manager --version v1.11.0 jetstack/cert-manager --create-namespace
```

## 13. Create the issuer

```bash
k apply -f ./issuer-prod.k8s.yaml
```

## 14. Update ingress

```bash
k apply -f ./ingress-production.k8s.yaml
```

## Test in the browser x boom
